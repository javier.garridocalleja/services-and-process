#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
   
#define MAX 10
#define MAXLADR 600
   
struct TAlmacen {
     int ladrillos = 0;
     int recogidos = 0;
};
  
void recogerLadrillos ( struct TAlmacen *alm) {
    int numero = 0;
    numero = rand () % MAX + 1;
  
    while(alm->recogidos < MAXLADR) {
        if(numero%2 == 0)
            alm->recogidos++;
        else
            alm->recogidos += 2;
  
        printf("El trabajador con identificador %li, recoge %i ladrillos\n", pthread_self(), alm->recogidos);
        usleep(100000);
    }
}
void* funcion_fabrica(void* args) {
      struct TAlmacen *alm = (struct TAlmacen*) args;
  
      while(alm->ladrillos < MAXLADR) {
          alm->ladrillos++;
          usleep(10000);
          printf("La fabrica tiene: %i ladrillos\n", alm->ladrillos);
      }
     printf("La fabrica cerro\n");
 
      return NULL;
}
 
void* funcion_obrero1(void* args) {
      struct TAlmacen *alm = (struct TAlmacen*) args;
  
      recogerLadrillos(alm);
  
      return NULL;
} 
  
void* funcion_obrero2 (void* args) {
      struct TAlmacen *alm = (struct TAlmacen*) args;
  
      recogerLadrillos(alm);
  
      return NULL;
}
  
 
void cerrar_fabrica(pthread_t th1, pthread_t th2) {
      pthread_join(th1, NULL);
      pthread_join(th2, NULL);
}
   
int main(int argc, char* argv[]) {
       struct TAlmacen alm;
   
       srand(time(NULL));
   
   
      pthread_t thread1_id;
      pthread_t thread2_id;
      pthread_t fabrica;
     
     pthread_create(&thread1_id, NULL, &funcion_obrero1, &alm);
     pthread_create(&thread2_id, NULL, &funcion_obrero2, &alm);
     pthread_create(&fabrica, NULL, &funcion_fabrica, &alm);
	
    cerrar_fabrica(thread1_id, thread2_id);

    return EXIT_SUCCESS;
}

