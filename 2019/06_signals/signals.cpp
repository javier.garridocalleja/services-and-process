#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

sig_atomic_t sigusr1_count = 0;

void handler (int signal_count) {
	++sigusr1_count; 
}

int main() {
	char order;
	struct signaction sa;

	memset(&sa, 0, sizeof(sa));
	sa.sa_handler = &handler;
	signaction (SIGUSR1, &sa, NULL);

	//scanf("%s", order);
	printf("SIGUSR1 was raised %d times\n", sigusr1_count);


	return 0;
}

