#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
  int n1,
      n2,
      resultado;

  if(argc < 3) {
      abort();
  }
  n1 = atoi(argv[1]);
  n2 = atoi(argv[2]);

  resultado = n1 + n2;

  return resultado;
}
