#ifndef __ARITMETICA_H_
#define __ARITMETICA_H_

#include <stdio.h>
#include <stdlib.h>

#ifdef _cplusplus
extern "C" {
#endif
int sumar(int numero1, int numero2);
int restar(int numero1, int numero2);
#ifdef _cplusplus
}
#endif

#endif
