#ifdef __LIBRERIA_H__
#define __LIBRERIA_H__

#include <stdio.h>
#include <stdlib.h>

#ifdef _cplusplus
extern "C" {
#endif
int sumar(int numero1, int numero2, int resultadosuma);
int restar(int numero1, int numero2, int resultadoresta);
int catalogo(int numero1, int numero2);

#ifdef _cplusplus
}
#endif

#endif

