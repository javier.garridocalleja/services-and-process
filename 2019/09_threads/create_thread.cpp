#include <pthread.h>
#include <stdio.h>

/* PARAMETROS PARA FUNCION */ 

struct parametros_estructura {
	// caracter a imprimir
	char caracter;
	// numero de veces que se va a imprimir
	int count;
};

/* FUNCION QUE IMPRIME UN CARACTER POR EL TUBO STDERR CON EL PUNTERO A LA ESTRUCTURA \"PARAMERTOS DE LA ESTRUCTURA\" */ 

void* imprimir_caracteres (void* parametros) {
	// hacemos un molde de la estructura
	struct parametros_estructura* p = (struct parametros_estructura *) parametros;
	int i;
	
	for(i=0; i< p->count; i++) {
		fputc(p->caracter, stderr);
	}
	return NULL;
}

int main() {
	// nombres que les vamos a poner a cada hilo
	pthread_t id_thread1;
	pthread_t id_thread2;
	
	// especificacion de los argumentos de la estructura con los parametros de la propia estructura
	struct parametros_estructura argumentos_thread1;
	struct parametros_estructura argumentos_thread2;

	// se crea un nuevo hilo 
	
	argumentos_thread1.caracter = 'x';
	argumentos_thread2.count = 3;
	pthread_create (&id_thread1, NULL, &imprimir_caracteres, &argumentos_thread1);

	//se crea el segundo hilo
	
	argumentos_thread2.caracter = 'o';
	argumentos_thread2.count = 3;
	pthread_create (&id_thread2, NULL, &imprimir_caracteres, &argumentos_thread2);

	return 0;


}

