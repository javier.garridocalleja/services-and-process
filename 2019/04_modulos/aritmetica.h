#ifdef __ARTIRMETICA_H__
#define __ARITMETICA_H__

#ifdef __cplusplus
extern "C" {
#endif

int sumar(int numero1, int numero2);
int restar(int numero1, int numero2);

#ifdef __cplusplus
}
#endif

#endif
