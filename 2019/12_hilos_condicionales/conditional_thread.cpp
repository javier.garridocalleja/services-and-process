#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

int thread_flag;
pthread_cond_t thread_flag_cv;
pthread_mutex_t thread_flag_mutex;

void initialize_flag() {
	// inizializar mutex y la variable de condicion
	pthread_mutex_init (&thread_flag_mutex, NULL);
	pthread_cond_init (&thread_flag_cv, NULL);
	// inizializar el valor del flag
	thread_flag = 0;
}


void do_work() {
	printf("Trabajo hecho correctamente\n");
}

// llama a do_work repetidamente mientras el hilo flag esta activo. Lo bloquea si el flag esta limpio

void* thread_function1(void* thread_arg) {
	while(1) {
		pthread_mutex_lock (&thread_flag_mutex);
		while(!thread_flag) {
			pthread_cond_wait(&thread_flag_cv, &thread_flag_mutex);
			pthread_mutex_unlock (&thread_flag_mutex);
			do_work();
			usleep(10000);
		}
		return NULL;
	}
}
void* thread_function2(void* thread_arg) {
	while(1) {
		pthread_mutex_lock (&thread_flag_mutex);
		while(!thread_flag) {
			pthread_cond_wait(&thread_flag_cv, &thread_flag_mutex);
			pthread_mutex_unlock (&thread_flag_mutex);
			do_work();
			usleep(10000);
		}
		return NULL;
	}
}
void set_thread_flag (int flag_value) {
	// se hace el lock del mutex  antes de acceder al valor flag
	pthread_mutex_lock (&thread_flag_mutex);
	
	thread_flag = flag_value;
	pthread_cond_signal (&thread_flag_cv);
	// desbloquea el mutex
	pthread_mutex_unlock (&thread_flag_mutex);
}


int main() {


	initialize_flag();
	
	// se crean los hilos
	pthread_t thread1_id;
	pthread_t thread2_id;

	pthread_create(thread1_id, NULL, &thread_function1, NULL);
	pthread_create(thread2_id, NULL, &thread_function2, NULL);

	// finalizamos los hilos
	pthread_join(thread1_id, NULL);
	pthread_join(thread2_id, NULL);

	return EXIT_SUCCESS;
}
