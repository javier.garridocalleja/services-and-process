#include <stdio.h>
#include <stdlib.h>
#include "libreria.h"

int sumar(int op1, int op2, int resultadosuma) {
	resultadosuma = op1 + op2;
	return resultadosuma;
}
int restar(int op1, int op2, int resultadoresta) {
	resultadoresta = op1 - op2;
	return resultadoresta;
}
int catalogo(int numero1, int numero2) {
        int resultadosuma, resultadoresta;
	resultadosuma = sumar(numero1, numero2, resultadosuma);
	resultadoresta = restar(numero1, numero2, resultadoresta);

	printf("Los resultados de la suma y de la resta entre %i y %i son %i y %i\n", 
		numero1, numero2, resultadosuma, resultadoresta);
}
