#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>

sig_atomic_t sigusr1_count = 0;

int spawn (char* program, char* arg_list[]) {
	pid_t child_id;
	child_id = fork();	
	if(child_id != 0) 
		return child_id;
	else {
		execvp(program, arg_list);
		fprintf(stderr, "No se ha podido ejecutar el programa %s\n", program);
		abort();
	}
	return child_id;
}

void contador (int signal) {
	sigusr1_count++;
}

int main(int argc, char* argv[]) {
	struct sigaction s;
	pid_t id;
	int child_status;
	char* arg_list[3];

	s.sa_handler = &contador;

	arg_list[0] = argv[1];
	arg_list[1] = argv[2];
	arg_list[2] = NULL;

	id = spawn (arg_list[0], arg_list);

	while(1) {
		printf("Hello World");
		sleep(1);
	}
	kill(id, SIGTERM);
	wait(&child_status);

	return 0;
}
