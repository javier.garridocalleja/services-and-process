#!/bin/bash

## SCRIPT PARA ACTUALIZAR LOS REPOSITORIOS LOCALES DE GITLAB EN LA UBICACION DESEADA

#PARA COLOCARLO EN EL CRONTAB SE HARIA DE LA SIGUIENTE MANERA
# mm hh * * * /bin/sh ruta/pull.sh
# mm y hh -> minuto y hora de ejecucion
# ruta -> lugar donde se encuentra el archivo ejecutable


echo "Introduce la ruta de su carpeta de git para actualizar"
read -p "ruta: " ruta
cd $ruta
git pull
echo "Su repositorio ha sido actualizado a fecha de $(date +%c)"
