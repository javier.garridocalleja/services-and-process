#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>

pthread_mutex_t mutex1 = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutex2 = PTHREAD_MUTEX_INITIALIZER;


// Estas funciones se van a ejecutar de manera concurrente
void* imprimir_i(void *ptr) {
  pthread_mutex_lock(&mutex1);
  pthread_mutex_lock(&mutex2);
  printf("Funcion i");
  pthread_mutex_unlock(&mutex2);
  pthread_mutex_unlock(&mutex1);
}

void* imprimir_j(void *ptr) {
  pthread_mutex_lock(&mutex2);
  pthread_mutex_lock(&mutex1);
  printf("Funcion j");
  pthread_mutex_unlock(&mutex1);
  pthread_mutex_unlock(&mutex2);
}

int main() {

  	pthread_t t1, t2;
	int iret1 = pthread_create(&t1, NULL, imprimir_i, NULL);
  	int iret2 = pthread_create(&t2, NULL, imprimir_j, NULL);

  while(1){}
  
  return EXIT_SUCCESS;
}
