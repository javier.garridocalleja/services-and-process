#include <stdlib.h>
#include <stdio.h>
#include <dlfcn.h>
#include <string.h>
#include "aritmetica.h"

int main(){
  void *handle;

  handle = dlopen("./libtest.so", RTLD_LAZY);
  void (*test)() = (void (*)()) dlsym(handle, "suma");
  (*test)();
  dlclose(handle);

    return EXIT_SUCCESS;
}
