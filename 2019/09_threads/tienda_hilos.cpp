#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#define DELAY 50000
#define V 100

#define FICHERO "hilos.txt"

struct TPila {
	int id_hilo;
	int count;
};

void menu() {
	printf("Bienvenido al supermercado, por favor introduzca un número\n");
	printf("1. Crear un hilo\n");
	printf("2. Cepillar el hilo\n");
	printf("3. Cerrar tienda\n");
}

void* imprimir_id (void* parametros) {
	struct TPila *p = (struct TPila *) parametros;
	for(int i=0; i< p->count; i++) {
		fputc(p->id_hilo, stderr);
	}
	return NULL;
}

void barra() {

	for(int veces=0; veces<=V; veces++){
              for(int columnas=0; columnas < veces; columnas++)
                  fprintf(stderr, "=");
	      fprintf(stderr, "Tratando hilo...%i%%\r", veces);
              usleep(DELAY);
            }
}

int main() {
	
	int opcion;
	menu();
	printf("Introduce una opcion: ");
	scanf("%i", &opcion);
	
	pthread_t id_thread1;
	pthread_t id_thread2;

	struct TPila args_thread1;
	struct TPila args_thread2;
		

	switch(opcion) {
		case 1: 
			barra();
			printf("\n");
			printf("Hilo creado correctamente\n");
			args_thread1.id_hilo = id_thread1;
			pthread_create(&id_thread1, NULL, &imprimir_id, &args_thread1);	
			//printf("Thread id = %li\n", id_thread1);
			FILE *pf;
			pf = fopen(FICHERO, "w");
			fprintf(pf, "ID %lu\n", pthread_self());
			fflush(pf);
    			//pthread_join(id_thread1, NULL);
			fclose(pf);
			break;
		case 2:
			barra();
			printf("Has seleccionado la opcion para cancelar el hilo\n");
			pthread_join(id_thread1, NULL);
			printf("Has cancelado correctamente el hilo %li\n", id_thread1);
			break;
		case 3:

			break;
		default:
			printf("Elija una opcion correcta [1 - 3]\n");
	}
	return 0;
}

