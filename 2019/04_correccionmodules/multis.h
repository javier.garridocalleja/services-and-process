#ifndef __MULTIS_H__
#define __MULTIS_H__


#ifdef __cplusplus
extern "C" {
#endif

    const char **catalogo();
    int mul (int op1, int op2);
    int div (int op1, int op2);

#ifdef __cplusplus
}
#endif



#endif

