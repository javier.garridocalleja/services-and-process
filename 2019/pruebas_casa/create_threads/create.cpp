#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>


void *threadfunct(void *vargp) {
	sleep(1);
	printf("Printing hello from thread\n");
	return NULL;
}

int main() {

	pthread_t thread_id;
	printf("Printing message... \n");	
	pthread_create(&thread_id, NULL, threadfunct, NULL);
	pthread_join(thread_id, NULL);

	return EXIT_SUCCESS;
}
