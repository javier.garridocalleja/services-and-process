#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

pid_t spawn (char* program, char ** arg_list) {
  pid_t child_pid;
  child_pid = fork();
  if(child_pid != 0) {
      return child_pid;
  }else {
      execvp(program, arg_list);
      fprintf(stderr, "Ha ocurrido un error al realizar el exec");
      abort();
  }
}

int main(int argc, char *argv[]) {
  pid_t child_pid;
  int child_status,
      resultado;

  child_pid = spawn("./suma", argv);
  wait(&child_status);

  if(WIFEXITED(child_status)) {
      resultado = WEXITSTATUS(child_status);
      printf("El resultado de la suma es %i\n", resultado);
  }
  printf("El resultado del programa fue exitoso\n");

  return EXIT_SUCCESS;

}
