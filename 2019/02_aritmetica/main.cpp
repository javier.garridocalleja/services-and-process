#include <stdio.h>
#include <stdlib.h>
#include "aritmetica.h"

/* Función punto de entrada*/


int main(int argc, char *argv[]){

	int numero1 = 5,
	    numero2 = 3;
	int resultadosuma, resultadoresta;

	resultadosuma = sumar(numero1, numero2);
	resultadoresta = restar(numero1, numero2);
	
	printf("El resultado de la suma de %i y %i es: %i\n", numero1, numero2, resultadosuma);
	printf("El resultado de la resta de %i y %i es: %i\n", numero1, numero2, resultadoresta);

	return EXIT_SUCCESS;
}
