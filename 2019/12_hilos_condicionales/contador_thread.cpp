#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>

int thread_flag;
pthread_cond_t thread_flag_cv;
pthread_mutex_t thread_flag_mutex = PTHREAD_MUTEX_INITIALIZER;

void initialize_flag() {
	pthread_mutex_init(&thread_flag_mutex, NULL);
	pthread_cond_init (&thread_flag_cv, NULL);

	thread_flag = 0;
}

void do_work() {
	int seg;
	printf("Han pasado %i segundos\n", seg);
}

void* thread_function (void* thread_arg) {
	while(1) {
		pthread_mutex_lock(&thread_flag_mutex);

		fflush(stdin);
		usleep(1000);

		pthread_mutex_unlock(&thread_flag_mutex);

		do_work();
	}
	return NULL;
}

void set_thread_flag (int flag_value) {

	pthread_mutex_lock(&thread_flag_mutex);

	thread_flag = flag_value;

	pthread_cond_signal (&thread_flag_cv);

	pthread_mutex_unlock(&thread_flag_mutex);
}

int main() {
	initialize_flag();

	pthread_t thread1_id;
	pthread_t thread2_id;

	pthread_create(&thread1_id, NULL, &thread_function, NULL);
	pthread_create(&thread2_id, NULL, &thread_function, NULL);

	pthread_join(thread1_id, NULL);
	pthread_join(thread2_id, NULL);

	return EXIT_SUCCESS;
}
